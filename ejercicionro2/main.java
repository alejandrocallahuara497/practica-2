package com.mycompany.ejercicionro2;
import javax.swing.JOptionPane;
public class main {
    public static void  main(String[] args){
        String inputlimit,inputtype,inputdata;
        inputlimit = JOptionPane.showInputDialog("Introduzca un limite de datos");
        inputtype = JOptionPane.showInputDialog("Que pila desea usar :\n -estatica \n -dinamica");
        int aux = 1;
        int limit = Integer.parseInt(inputlimit);
        String result = "";
        switch(inputtype){
            case "dinamica":
                stackDinamic pd = new stackDinamic();
                do{
                    inputdata = JOptionPane.showInputDialog("Introduzca un numero entero");
                    if(inputdata.length() == 0)
                        inputdata = JOptionPane.showInputDialog("Introduzca un numero entero");
                    else
                        pd.push(Integer.parseInt(inputdata));
                    aux++;
                }while(aux <= limit);
                stackDinamic resultpd = dinamicNumbers(pd);
                while(!resultpd.isEmpty()){
                    result = result +'-'+resultpd.pop();
                }
                JOptionPane.showMessageDialog(null, "resultado de pila dinamica:\n"+result);
            break;
            case "estatica":
                stackStatic ps = new stackStatic(limit);
                do{
                    inputdata = JOptionPane.showInputDialog("Introduzca un numero entero");
                    if(inputdata.length() == 0)
                        inputdata = JOptionPane.showInputDialog("Introduzca un numero entero");
                    else
                        ps.push(Integer.parseInt(inputdata));
                    aux++;
                }while(aux <= limit);
                
                stackStatic resultps = stackNumbers(ps,limit);
                while(!resultps.isEmpty()){
                    result = result +'-'+resultps.pop();
                }
                JOptionPane.showMessageDialog(null, "resultado de pila estatica:\n"+result);
            break;
            default:
                
                JOptionPane.showMessageDialog(null, "Escoja un valor valido");
            break;
            
            
        }
    }
    public static stackDinamic dinamicNumbers(stackDinamic pd){
            stackDinamic p1d = new stackDinamic();
            stackDinamic p2d = new stackDinamic();
            while(!pd.isEmpty()){ 
                int temp = pd.pop();
                int count = 1;     
                while(!pd.isEmpty()){
                    int temp2 = pd.pop();
                    if(temp2 == temp){
                        count++;
                    }else{
                        p2d.push(temp2);
                    }
                }
                if(count == 1){
                    p1d.push(temp);
                }
                while(!p2d.isEmpty()){
                    pd.push(p2d.pop());
                }
            }
            return p1d;
    }
    
    public static stackStatic stackNumbers(stackStatic ps, int limit){
        stackStatic p1 = new stackStatic(limit);
        stackStatic p2 = new stackStatic(limit);
        while(!ps.isEmpty()){
            int temp = ps.pop();
            int count = 1;        
            while(!ps.isEmpty()){
                int temp2 = ps.pop();
                if(temp2 == temp){
                    count++; 
                }else{
                    p2.push(temp2);
                }
            }
            if(count == 1){
                p1.push(temp);
            }
            while(!p2.isEmpty()){
                ps.push(p2.pop());
            }
        }
        return p1;
    }
    
    
    
}
